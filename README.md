# CI/CD Example

## What's new?

There is no features at the moment.

## About project

This project aims to learn CI/CD workflow, using GitLab's tools.

## Technologies used in the project

- Angular CLI v13

## Goals

Like I mentioned before, this project was created to learn about CI/CD tools offered by GitLab service.

## License

My project is under BSD-2 Clause license. You can use some parts from my code but remember to credit me as an appreciation to my work! Thanks! Awoo!

© 2022 Paweł "Wilczeq/Vlk" Turoń
